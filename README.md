# FGV SUSEP

## Guia rápido

* Clone ou baixe este repositório Git em seu computador.
* Instale o [Node.js](https://nodejs.org/en/) se ainda não o tiver.
* Execute `npm install`.
* Execute `gulp` para executar a tarefa Gulp padrão.
* Obervações: O comando `gulp` já cria a pasta build, gerando os arquivos finais compilados. A pasta build fica oculta no .gitignore, junto com a pasta node_modules.