// window.cookieconsent.initialise({
// 	container: document.getElementById("cookieconsent"),
// 	palette: {
// 		popup: {
// 			background: "#004156",
// 			text: "#FFFFFF",
// 			link: "#45D09E",
// 		},
// 		button: {
// 			background: "#041E33",
// 			border: "transparent",
// 			text: "#FFFFFF",
// 		},
// 		highlight: {
// 			background: "#041E33",
// 			border: "transparent",
// 			text: "#0C1319",
// 		},
// 	},
// 	content: {
// 		header: "Cookies used on the website!",
// 		message: "Este site usa cookies para garantir que você obtenha a melhor experiência em nosso site.",
// 		allow: "Aceitar",
// 		deny: "Recursar",
// 		dismiss: "Concordo",
// 		link: "Saiba mais",
// 		href: "https://www.osano.com/legal/cookies",
// 		policy: "Política de Cookie",
// 	},
// 	revokable: true,
// 	onStatusChange: function(status) {
// 	  console.log(this.hasConsented() ?
// 	  'enable cookies' : 'disable cookies');
// 	},
// 	"theme": "edgeless",
// 	position: "bottom"
// });