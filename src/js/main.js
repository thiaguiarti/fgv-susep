// bootstrap poppovers
$(function () {
    $('[data-toggle="tooltip"]').tooltip();
    $('[data-toggle="popover"]').popover();
});

// modal
$('.modal').modal(
    {
        backdrop: 'static',
        keyboard: false,
        show: false
    }
);

// menu
$('.sidebarIconToggle').on(
    'click', function() {
        $(this).toggleClass('open');
        if($(this).hasClass('open')) {
            // $('body').css('overflow', 'hidden');
        } else {
            // $('body').css('overflow', 'inherit');
        }
    }
);

// steps
var stepContainer = {
    init: function() {
        this.engine();
    },
    engine: function() {
        var form    = $('form');
        var step    = $('form .step');
        var button  = step.find('.go');
        var backstep  = step.find('#back-to-step');

        // console.log(button);
 
        button.on (
            "click", function() {
                
                var stepSelected    = $(this).parents('.step');
                var input           = stepSelected.find('input');
                var ID              = $(this).parents('.step').attr('id');
                var area            = $(this).attr('data-step');
                var result;
                
                // console.log(ID);

                input.each(function() {
                    var value = $(this).val();

                    if(value == '') {
                        form.addClass('was-validated');
                        // console.log('vazio');
                        result = 'vazio';
                        return false;
                    }
                });

                if(result != 'vazio') {
                    step.removeClass('opened').removeAttr('style');
                    $("."+area+"").fadeIn();
                    // console.log($('#'+area));
                }
            }
        ),
        backstep.on(
            "click", function() {
                var area = $(this).attr('data-step');

                // console.log(area);

                step.removeAttr('style');
                $("."+area+"").fadeIn();
            }
        )
    }
}

// edit profile validate
var editProfileValidate = {
    init: function() {
        this.engine();
    },
    engine: function() {
        var form    = $('form');
        var step    = $('form .stepEdit');
        var button  = step.find('.go');

        button.on (
            "click", function() {
                
                var stepSelected    = $(this).parents('.stepEdit');
                var input           = stepSelected.find('input');

                input.each(function() {
                    var value = $(this).val();

                    if(value == '') {
                        form.addClass('was-validated');
                        return false;
                    }
                });
            }
        )
    }
}
editProfileValidate.init();

// profile content user
var optionUser = {
    init: function() {
        this.engine();
    },
    engine: function() {
        var el = $('.option-user a'),
            container = $('.option-user-container');

        el.on(
            'click',
            function() {
                $('.option-user a').removeClass('actived');
                $(this).addClass('actived');
                var hash = $(this).attr('id');
                // console.log(hash);
                container.addClass('d-none');
                $('.option-user-container.' + hash + '').removeClass('d-none');
            }
        )
    }
}
optionUser.init();