// /*
// ####################################################
// VALIDATE FORM
// ####################################################
// */

//Email
$(".email").inputmask({
	mask: "*{1,20}[.*{1,20}][.*{1,20}][.*{1,20}]@*{1,20}[.*{2,6}][.*{1,2}]",
	greedy: false,
	onBeforePaste: function (pastedValue, opts) {
		pastedValue = pastedValue.toLowerCase();
		return pastedValue.replace("mailto:", "");
	},
	definitions: {
		'*': {
		validator: "[0-9A-Za-z!#$%&'*+/=?^_`{|}~\-]",
		casing: "lower"
		}
	}
})

// CPF
$(".cpf").inputmask("999.999.999-99");

// CNPJ
$(".cnpj").inputmask("99.999.999/9999-99");

// RG
$(".rg").inputmask("decimal", 
	{
		// groupSeparator: ",",
		rightAlign: false,
		max: 99999999999999
	}
);

// Fixo
$(".fixo").inputmask("(99) 9999-9999");

// Telefone
$(".telefone").inputmask("(99) 99999-9999");

// CEP
$(".cep").inputmask("99999-999");

// Data Nascimento
$(".dtnascimento").inputmask("99/99/9999");

// CPF CNPJ
$("input[id*='cpfcnpj']").inputmask({
	mask: ['999.999.999-99', '99.999.999/9999-99'],
	keepStatic: true
});